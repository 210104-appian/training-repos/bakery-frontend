const baseUrl = "http://localhost:8082/java-bakery-server";

const itemsUrl = baseUrl + "/items";
const breadUrl = itemsUrl + "?type=bread";
const muffinUrl = itemsUrl + "?type=muffin";

/*
performAjaxGetRequest(breadUrl, function(json){
    let bread = JSON.parse(json);
    console.log(bread);
})
*/


function performAjaxGetRequest(url, callback){
    const xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.onreadystatechange = function(){
        if(xhr.readyState==4){
            if(xhr.status==200){
                callback(xhr.response); // this is going to be the response body of our http response  (JSON)
            } else {
                console.error("something went wrong with our GET request to "+url);
            }
        } 
    }
    xhr.send();
}

function getAllBread(callback){
    performAjaxGetRequest(breadUrl, callback);
}

function getAllMuffins(callback){
    performAjaxGetRequest(muffinUrl, callback);
}

function performAjaxPostRequest(url, callback, payload){
    const xhr = new XMLHttpRequest();
    xhr.open("POST", url);
    xhr.onreadystatechange = function(){
        if(xhr.readyState==4 ){
            if(xhr.status==201){
                callback(xhr.response); // this is going to be the response body of our http response  (JSON)
            } else {
                console.error("An error occurred while attempting to create a new record")
            }
        }
    }
    xhr.send(payload);
}