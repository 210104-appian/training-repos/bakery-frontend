var x = 5;
var x = 10;

let y = "Hello";
//let y = "World"; // this would give us an error
y = x;
y = true;

const z = 10;
// z = 3; // this would give me an error 

let cat = {
    name: "Ferdinand",
    breed: "calico",
    age: 2,
    meow: function(){
        console.log(this.name+" meows")
    }
  };
  

class Cat{
    constructor(name, breed){
        this.name = name;
        this.breed = breed;
    }
}

function CatConstructor(name, breed){
    this.name = name;
    this.breed = breed;
}

function printAll(){
    for(let num of arguments){
        console.log(num);
    }
}

function returnObject(){
    return 
    {
        message: "Hello"
    }
}