// const breads = [
//   { id: 5, price: 2.5, type: "BAGUETTE" },
//   { id: 6, price: 3.0, type: "SOURDOUGH" },
//   { id: 7, price: 2.75, type: "PUMPERNICKEL" },
// ];

let breads;

// what we should get from our Java application if we asked for all of the breads in the database
// this is JSON (JavaScript Object Notation) - a language agnostic data format often used to transfer data

let muffins;

const coffeetypes = [
  { id: 8, price: 2.75, size: "S" },
  { id: 9, price: 3.0, size: "M" },
  { id: 10, price: 3.25, size: "L" },
];

let cart = [];

function capitalizeWord(word) {
  return word.charAt(0).toUpperCase() + word.substring(1).toLowerCase();
}

let tableFooter = document.querySelector(".table-footer");
let chechOutBtn = document.querySelector(".checkout-button");
let paymentOptions = document.querySelector(".payment-options");

window.onload = function () {
  if (cart.length == 0) {
    tableFooter.style.display = "none";
    // chechOutBtn.style.display = "none";
    paymentOptions.style.display = "none";
  }

  /*
  these methods send an HTTP response and take in a callback function which define how we should process the JSON returned from the HTTP response
  */
  getAllBread(renderBreadOptions);

  getAllMuffins(renderMuffinOptions);

  for (let coffee of coffeetypes) {
    // console.log(coffee);
  }
};

function renderMuffinOptions(jsonMuffins){

  muffins = JSON.parse(jsonMuffins);

  for (let muffin of muffins) {
    let muffinCheckbox = document.createElement("input");
    muffinCheckbox.value = muffin.id;
    muffinCheckbox.type = "checkbox";
    muffinCheckbox.className = "muffin-checkbox";

    let breakLine = document.createElement("br");

    let muffinLabel = document.createElement("label");
    muffinLabel.innerText =
      capitalizeWord(muffin.flavor) + " - $" + muffin.price.toFixed(2);
    let muffinSelections = document.getElementById("muffin-selection");

    muffinSelections.append(muffinCheckbox, muffinLabel, breakLine);
  }
}

function renderBreadOptions(jsonBread){
  
  breads = JSON.parse(jsonBread);

  for (let bread of breads) {
    let breadCheckbox = document.createElement("input");
    breadCheckbox.value = bread.id;
    breadCheckbox.type = "checkbox";
    breadCheckbox.className = "bread-checkbox";

    let breadLabel = document.createElement("label");
    breadLabel.innerText =
      capitalizeWord(bread.type) + " - $" + bread.price.toFixed(2);

    let breakLine = document.createElement("br");

    let breadSelections = document.getElementById("bread-selections");
    breadSelections.append(breadCheckbox, breadLabel, breakLine);
  }
}

document.getElementById("add-btn").addEventListener("click", function (event) {
  event.preventDefault();
  let breadBoxes = document.getElementsByClassName("bread-checkbox");
  let muffinBoxes = document.getElementsByClassName("muffin-checkbox");

  for (let box of breadBoxes) {
    if (box.checked) { // looking at each checkbox to determine which bread has been selected
      let selectedBread = findBreadById(box.value);
      let itemInCart = findItemInCart(selectedBread.id); // return a valid object in our cart or undefined
      if(itemInCart){ // because of type coercion, we are able to use any value as a boolean
        itemInCart.qty++;
        // update quantity of an existing record in the cart table
      } else{
        let newCartItem = {...selectedBread, qty: 1}; // spread operator makes a copy of selected bread, we also add the qty field
        cart.push(newCartItem);
        addSelectedItemToCart(selectedBread);
      }
      box.checked = false;
    }
  }
  console.log(cart);

  for (let box of muffinBoxes) {
    if (box.checked) {
      let selectedMuffin = findMuffinById(box.value);
      let itemInCart = findItemInCart(selectedMuffin.id);
      if(itemInCart){
        itemInCart.qty++;
      } else {
        let newCartItem = {...selectedMuffin, qty:1};
        cart.push(newCartItem);
        addSelectedItemToCart(selectedMuffin);
      }
      box.checked = false;
    }
  }

  recalculateTotal();
});

function addSelectedItemToCart(item) {
  let newRow = document.createElement("tr");

  let removeItemBtn = document.createElement("button");
  removeItemBtn.innerHTML = "x";
  removeItemBtn.id = "rm-btn-"+item.id;

  removeItemBtn.className = "btn btn-danger remove-btn";
  let itemColumn = document.createElement("td");
  itemColumn.innerText =  item.type || item.flavor;
  let itemPrice = document.createElement("td");
  itemPrice.innerText = item.price.toFixed(2);
  let itemQuantity = document.createElement("td");

  let quantityInput = document.createElement("input");
  quantityInput.type = "number";
  quantityInput.id = "user-qty-input";
  quantityInput.min = 1;
  quantityInput.max = 100;
  quantityInput.value = 1;

  quantityInput.value = 1;
  itemQuantity.appendChild(quantityInput);
  let itemSubtotal = document.createElement("td");
  itemSubtotal.innerText = (item.price * quantityInput.value).toFixed(2);
  quantityInput.addEventListener("change", function () {
    itemSubtotal.innerText = (item.price * quantityInput.value).toFixed(2);

    recalculateTotal();
  });

  newRow.append(
    itemColumn,
    itemPrice,
    itemQuantity,
    itemSubtotal,
    removeItemBtn
  );
  document.getElementById("cart-table-body").appendChild(newRow);
  tableFooter.style.display = "block";
  //   chechOutBtn.style.display = "block";
  paymentOptions.style.display = "block";

  removeItemBtn.addEventListener("click", function () {
    deleteRow(removeItemBtn);
  });
}

function recalculateTotal() {
  let total = 0;
  for (let item of cart) {
    total += item.price*item.qty;
  }
  document.getElementById("total-cart-amount").innerText =
    "$" + total.toFixed(2);
}

function findBreadById(id) {
  for (let bread of breads) {
    if (bread.id == id) {
      return bread;
    }
  }
}

function findItemInCart(id){
  for(let item of cart){
    if(item.id===id){
      return item;
    }
  }
  return null;
}

function findMuffinById(id) {
  for (let muffin of muffins) {
    if (muffin.id == id) {
      return muffin;
    }
  }
}

function deleteRow(btn) {
  var row = btn.parentNode;
  row.parentNode.removeChild(row);

  let removeItemId = btn.id.substring(7);
  console.log(removeItemId);

  // let myFilterFunction = cartItem=>cartItem.id!=removeItemId;

  cart = cart.filter(cartItem=>cartItem.id!=removeItemId);

  recalculateTotal();

  // console.log(cart)
  if (cart.length === 0) {
    tableFooter.style.display = "none";
  }
}
