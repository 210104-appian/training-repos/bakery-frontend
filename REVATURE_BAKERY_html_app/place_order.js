document.getElementById("order-btn").addEventListener("click", placeOrder);

function placeOrder(){
    // construct an object to represent our order
    let hasPrepaid = false;
    let payOptions = document.getElementsByName("pay-option");
    for(let payOption of payOptions){
        if(payOption.checked){
            if(payOption.id!="in-store"){
                hasPrepaid = true;
            }
        }
    }

    let order = {hasPrepaid, items: cart, status: "PENDING"};

    // convert order to JSON
    const orderJson = JSON.stringify(order);
    
    // send JSON via AJAX -- baseURL and method declaration can be found in bakery_data.js
    performAjaxPostRequest(baseUrl+"/orders",respondToSuccessfulOrderCreation, orderJson);
}

function respondToSuccessfulOrderCreation(){
    console.log("yay! we added a new order to our database")
}
